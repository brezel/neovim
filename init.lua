-- Load plugins managed by packer.nvim
require('plugins')
-- Load custom keymaps
require('keymaps.init')
-- Load Neovim and plugins settings
require('config.init')

-- Display numbers
vim.opt.number = true

-- Display hidden characters
vim.opt.listchars = {
	eol = '',
	tab = ' ',
	space = '·',
	nbsp = '_'
}
vim.opt.list = true
