local telescope = require('telescope.builtin')

vim.g.mapleader = '.'

vim.api.nvim_set_keymap('n', '<leader>ff', '', {
	noremap = true,
	callback = telescope.find_files
})

vim.api.nvim_set_keymap('n', '<leader>fg', '', {
	noremap = true,
	callback = telescope.live_grep
})

vim.api.nvim_set_keymap('n', '<leader>fb', '', {
	noremap = true,
	callback = telescope.buffers
})

vim.api.nvim_set_keymap('n', '<leader>fh', '', {
	noremap = true,
	callback = telescope.tags
})
