vim.api.nvim_set_keymap('n', '<C-Up>', ':bn <CR>', {
	noremap = true,
})

vim.api.nvim_set_keymap('n', '<C-Down>', ':bp <CR>', {
	noremap = true,
})

