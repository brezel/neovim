vim.api.nvim_set_keymap('n', '<C-Right>', ':tabnext <CR>', {
	noremap = true,
})

vim.api.nvim_set_keymap('n', '<C-Left>', ':tabprev <CR>', {
	noremap = true,
})

