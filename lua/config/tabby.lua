-- Force tabline to be shown
vim.o.showtabline = 2

require('tabby.tabline').use_preset('active_wins_at_tail', {
  tab_name = {
      name_fallback = function(tabid)
	return ''
      end,
  },
  buf_name = {
      mode = 'tail',
  },
})

local colors = {
  gray       = '#44475a',
  lightgray  = '#5f6a8e',
  white      = '#f8f8f2',
  black      = '#282a36',
}

local theme = {
  fill = { bg = colors.gray, fg = colors.black, gui = 'bold' },
  tab = { bg = colors.gray, fg = colors.white },
  current_tab = { bg = colors.lightgray, fg = colors.white, gui = 'bold' },
  win = { bg = colors.gray, fg = colors.white, gui = 'bold' },
}

require('tabby.tabline').set(function(line)
return {
    line.tabs().foreach(function(tab)
      local hl = tab.is_current() and theme.current_tab or theme.tab
      return {
        ' ' .. tab.number(),
        tab.name(),
        tab.close_btn(''),
        line.sep('', hl, theme.fill),
        hl = hl,
        margin = ' ',
      }
    end),
    line.spacer(),
    line.wins_in_tab(line.api.get_current_tab()).foreach(function(win)
      return {
        win.buf_name() .. ' ',
        hl = theme.win,
        margin = ' ',
      }
    end),
    hl = theme.fill,
  }
end)
