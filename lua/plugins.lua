vim.cmd [[packadd packer.nvim]]

return require('packer').startup({
	function(use)
		-- Package manager
		use 'wbthomason/packer.nvim'
		-- Colorscheme
		use 'Mofiqul/dracula.nvim'
		-- Better tabline
		use 'nanozuki/tabby.nvim'
		-- Fast file finder
		use {
			'nvim-telescope/telescope.nvim', tag = '0.1.0',
			requires = { {'nvim-lua/plenary.nvim'} }
		}
		-- Better line
		use {
			'nvim-lualine/lualine.nvim',
			requires = { 'kyazdani42/nvim-web-devicons', opt = true }
		}

	end,
	config = {
		display = {
			-- Open Packer in a floating window
			open_fn = require('packer.util').float,
		}
	}
})
