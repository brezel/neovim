.PHONY: bootstrap install uninstall

bootstrap:
	git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

install:
	ln -s `pwd` ~/.config/nvim

uninstall:
	rm ~/.config/nvim
